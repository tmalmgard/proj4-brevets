ACP time calculator

This is a brevet calculator for randonneurs using the international standard algorithm. 
#For users
Fill in the length of your race with the drop-down bar and fill in the start date and time for your race. Then fill in the distance of your control points either in miles or kilometers and press enter

The opening and close times will appear in the boxes to the right of the input time. Location name is optional. 

Note: You should not put control points closer than 30 km from the start. These will close before the start opens. 

#For developers
The template calc.html gathers the start time, length of the race and control point distances. These are calculated to kilometers if neccesary and sent by json to the flask server acp_flask.py. This script the calls acp_times.py to do the calculations of the open and close times. The open and close times is sent back to the template with ajax.

Work in progress. The backend works but not the AJAX insertion.  


Author: Torsten Malmgard

email: torstenm@uoregon.edu
