import arrow
import nose
import acp_times


def test_first:
    assert acp_times.open_time(275, 600, arrow.utcnow().isoformat()) == arrow.utcnow().shift(hours=8, minutes=14)
