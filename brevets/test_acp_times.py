import arrow
import nose
import acp_times


def test_open_short():
    start_time = arrow.get('2013-09-29T00:00:00.000000')
    open_time = start_time.shift(hours=2, minutes=56)
    assert acp_times.open_time(100, 200, start_time.isoformat()) == open_time.isoformat()

def test_close_long():
    start_time = arrow.get('2013-09-29T00:00:00.000000')
    close_time = start_time.shift(days = 2, hours=18, minutes=5)
    assert acp_times.close_time(898, 1000, start_time.isoformat()) == close_time.isoformat()

def test_open_past_goal():
    start_time = arrow.get('2013-09-29T00:00:00.000000')
    open_time = start_time.shift(days= 1, hours=9, minutes=5)
    
    assert acp_times.open_time(1005, 1000, start_time.isoformat()) == open_time.isoformat()

    
def test_close_short():
    start_time = arrow.get('2013-09-29T00:00:00.000000')
    close_time = start_time.shift(hours=9, minutes=32)

    assert acp_times.close_time(143, 1000, start_time.isoformat()) == close_time.isoformat()

def testi_open_on_border():
    start_time = arrow.get('2013-09-29T00:00:00.000000')
    open_time = start_time.shift(hours=5, minutes=53)
    assert acp_times.open_time(200, 600, start_time.isoformat()) == open_time.isoformat()
 
def test_open_long():
    start_time = arrow.get('2013-09-29T00:00:00.000000')
    open_time = start_time.shift(hours=11, minutes=49)
    assert acp_times.open_time(390, 600, start_time.isoformat()) == open_time.isoformat()

def test_close_medium():
    start_time = arrow.get('2013-09-29T00:00:00.000000')
    close_time = start_time.shift(days = 1, hours=2)

    assert acp_times.close_time(390, 600, start_time.isoformat()) == close_time.isoformat()


   
